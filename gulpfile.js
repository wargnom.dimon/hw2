let project_folder = "dist";
let source_folder = "src";
let build_folder = "build";
let fs = require("fs");

let path = {
  dist: {
    html: project_folder + "/",
    html_parts: project_folder + "/html/",
    css: project_folder + "/css/",
    reset_css: project_folder + "/css/reset.css",
    js: project_folder + "/js/",
    img: project_folder + "/img/",
    fonts: project_folder + "/fonts/",
  },
  build: {
    html: build_folder + "/",
    css: build_folder + "/css/",
    js: build_folder + "/js/",
    img: build_folder + "/img/",
    fonts: build_folder + "/fonts/",
  },
  src: {
    html: [source_folder + "/*.html", "!" + source_folder + "/html/_*.html"],
    html_parts: source_folder + "/html/*.html",
    css: source_folder + "/scss/style.scss",
    css_reset: source_folder + "/css/reset.css",
    js: source_folder + "/js/script.js",
    img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico,webp}",
    fonts: source_folder + "/fonts/*.ttf",
  },
  watch: {
    html: source_folder + "/**/*.html",
    css2: source_folder + "/css/**/*.css",
    css: source_folder + "/scss/**/*.scss",
    js: source_folder + "/js/**/*.js",
    img: source_folder + "/img/**/*.{jpg,png,svg,gif,ico, webp}",
  },
  clean: "./" + project_folder + "/",
  clean: "./" + build_folder + "/",
};

let { src, dest } = require("gulp");
gulp = require("gulp");
browsersync = require("browser-sync").create();
fileinclude = require("gulp-file-include");
del = require("del");
scss = require("gulp-sass");
autoPrefixer = require("gulp-autoprefixer");
group_media = require("gulp-group-css-media-queries");
clean_css = require("gulp-clean-css");
rename = require("gulp-rename");
uglify = require("gulp-uglify-es").default;
imagemin = require("gulp-imagemin");
webp = require("gulp-webp");
webphtml = require("gulp-webp-html");
webpcss = require("gulp-webp-css");
svgSprite = require("gulp-svg-sprite");
ttf2woff = require("gulp-ttf2woff");
ttf2woff2 = require("gulp-ttf2woff2");

function browserSync(params) {
  browsersync.init({
    server: {
      baseDir: "./" + project_folder + "/",
    },
    port: 3000,
    notify: false,
  });
}

function html() {
  return src(path.src.html)
    .pipe(fileinclude())
    .pipe(webphtml())
    .pipe(dest(path.build.html))
    .pipe(dest(path.dist.html))
    .pipe(browsersync.stream());
}

function html_parts() {
  return src(path.src.html_parts)
    .pipe(fileinclude())
    .pipe(webphtml())
    .pipe(dest(path.dist.html_parts))
    .pipe(browsersync.stream());
}

function images() {
  return src(path.src.img)
    .pipe(webp({ quaity: 70 }))
    .pipe(dest(path.build.img))
    .pipe(src(path.src.img))
    .pipe(
      imagemin({
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        interlaced: true,
        optimizationlevel: 3,
      })
    )
    .pipe(dest(path.dist.img))
    .pipe(browsersync.stream());
}

function fonts() {
  src(path.src.fonts)
    .pipe(ttf2woff())
    .pipe(dest(path.build.fonts))
    .pipe(dest(path.dist.fonts));
  return src(path.src.fonts)
    .pipe(ttf2woff2())
    .pipe(dest(path.build.fonts))
    .pipe(dest(path.dist.fonts));
}

function fontsStyle() {
  let file_content = fs.readFileSync(source_folder + "/scss/fonts.scss");
  if (file_content == "") {
    fs.writeFile(source_folder + "/scss/fonts.scss", "", cb);
    return fs.readdir(path.dist.fonts, function (err, items) {
      if (items) {
        let c_fontname;
        for (var i = 0; i < items.length; i++) {
          let fontname = items[i].split(".");
          fontname = fontname[0];
          if (c_fontname != fontname) {
            fs.appendFile(
              source_folder + "/scss/fonts.scss",
              '@include font("' +
                fontname +
                '", "' +
                fontname +
                '", "400", "normal");\r\n',
              cb
            );
          }
          c_fontname = fontname;
        }
      }
    });
  }
}

function cb() {}

function css() {
  return src(path.src.css)
    .pipe(scss({ outputStyle: "expanded" }))
    .pipe(group_media())
    .pipe(
      autoPrefixer({
        overrideBrowserslist: ["last 5 versions"],
        casscade: true,
      })
    )
    .pipe(webpcss())
    .pipe(src(path.src.css_reset))
    .pipe(dest(path.dist.css))
    .pipe(clean_css())
    .pipe(rename({ extname: ".min.css" }))
    .pipe(dest(path.build.css))
    .pipe(src(path.src.css))
    .pipe(dest(path.dist.css))
    .pipe(browsersync.stream());
}

function js() {
  return src(path.src.js)
    .pipe(fileinclude())
    .pipe(uglify())
    .pipe(rename({ extname: ".min.js" }))
    .pipe(dest(path.build.js))
    .pipe(src(path.src.js))
    .pipe(dest(path.dist.js))
    .pipe(browsersync.stream());
}

gulp.task("svgSprite", function () {
  return gulp
    .src([source_folder + "iconssprite/*.svg"])
    .pipe(svgSprite({ mode: { stack: { sprite: "../icons/icons.svg" } } }))
    .pipe(dest(path.build.img))
    .pipe(dest(path.dist.img));
});

function watchFiles(params) {
  gulp.watch([path.watch.html], html);
  gulp.watch([path.watch.css], css);
  gulp.watch([path.watch.css2], css);
  gulp.watch([path.watch.js], js);
  gulp.watch([path.watch.img], images);
}

function clean(params) {
  return del(path.clean);
}

let build = gulp.series(
  clean,
  gulp.parallel(js, css, html, images, fonts),
  fontsStyle
);

let dist = gulp.series(
  clean,
  gulp.parallel(js, css, html, html_parts, images, fonts),
  fontsStyle
);

let watch = gulp.parallel(dist, watchFiles, browserSync);

exports.fontsStyle = fontsStyle;
exports.fonts = fonts;
exports.images = images;
exports.js = js;
exports.css = css;
exports.html = html;
exports.html_parts = html_parts;
exports.build = build;
exports.dist = dist;
exports.watch = watch;
exports.default = watch;
